Hello! 

I wrote these scripts in 2006 and distribute them now over Gitlab. 

I am aware that structure, format and documentation are not in line with current standards. However, instead of never having time to prettify these aspects, I put the scripts into the public as they are. 

Please see the file mi_Rfunction.html or mi_Rfunction.txt for more information. 

Update 2024-12-28:
There is a recent Python implementation by Yannick Mahlich: https://github.com/pnnl-predictive-phenomics/bspline_mutual_information 

Best,
Carsten (carsten.daub@ki.se) 
2016-12-22
